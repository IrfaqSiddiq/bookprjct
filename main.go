package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

var db *sql.DB
var err error

type sandbox struct {
	isbn   string
	title  string
	author string
	price  float64
	uid    int
}

func init() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		"localhost", 5432, "postgres", "123456", "project")

	// connstr := "project://project:123456@localhost/postgreSQl?sslmode=disable"
	DB, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	if err = DB.Ping(); err != nil {
		panic(err)
	}
	db = DB
	fmt.Println("Now we are connected to postgresql")
}

func main() {
	http.HandleFunc("/data", datarecord)
	http.ListenAndServe(":8080", nil)
}
func datarecord(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Great!! we are connected to browser \n")
	if r.Method != "GET" {
		http.Error(w, http.StatusText(404), http.StatusMethodNotAllowed)
		return
	}
	rows, err := db.Query("Select * from books")

	if err != nil {
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}
	defer rows.Close()
	snbs := make([]sandbox, 0)

	for rows.Next() {
		snb := sandbox{}
		err := rows.Scan(&snb.isbn, &snb.title, &snb.author, &snb.price, &snb.uid)
		if err != nil {
			log.Println(err)
			http.Error(w, http.StatusText(500), 500)
			return
		}
		snbs = append(snbs, snb)
		for _, snb := range snbs {
			fmt.Fprintf(w, "%s,%s,%s,%f,%d\n", snb.isbn, snb.title, snb.author, snb.price, snb.uid)
		}
	}
}
